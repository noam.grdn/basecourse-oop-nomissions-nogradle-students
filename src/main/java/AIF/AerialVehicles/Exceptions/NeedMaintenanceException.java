package AIF.AerialVehicles.Exceptions;

public class NeedMaintenanceException extends AVException {
    public NeedMaintenanceException() {
        super("Need maintenance.");
    }
}
